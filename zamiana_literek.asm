.data
bufor: .byte 100
.text
.globl main
main:
	li $v0, 8
	la $a0, bufor
	la $a1, 100
	syscall
	la $t0, bufor #pointer to stored element
	move $t1, $t0 #pointer to analyzed element
loop:
	lbu $t2, ($t1)
	bltu $t2, ' ', end_loop
	
sprawdzenie:
	bltu $t2, 'A', loop_increment
	bgtu $t2, 'z', loop_increment
	bleu $t2, 'Z', zapis 
	
redukcja: 
	subiu $t2, $t2, ' '
	b sprawdzenie
	
zapis:
	sb $t2, ($t0)
	addiu $t0, $t0, 1
	
loop_increment:
	addiu $t1, $t1, 1
	b loop
	
end_loop:
	sb $zero, ($t0)
	li $v0, 4
	la $a0, bufor
	syscall
	li $v0, 10
	syscall